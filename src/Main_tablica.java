public class Main_tablica {
    public static void main(String[] args) {

        double jednaLiczba = 2.3;

        double[] tablica = new double[5];
        // przypisanie do pierwszej komórki w tablicy wartości 5.0
        tablica[0] = 5.0; // element pierwszy
        tablica[1] = 6.5; // element drugi
        tablica[2] = 1.1; // ... trzeci
        tablica[3] = 3.7; // ... czwarty
        tablica[4] = 9.9;  // ... piąty

        //Wewnątrz pętli napiszemy tekst "W komóce o indeksie [indeks] wartość: [wartość"
        for (int i = 0; i < 5; i++) {
            System.out.println("W komórce o indeksie " + i + " wartość: " + tablica[i]);
        }


    }
}
