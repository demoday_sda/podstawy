import java.util.Scanner;

public class Main_kalkulator {
    public static void main(String[] args) {
        // 0. Przygotowanie scannera - stworzenie obiektu
        Scanner konsola = new Scanner(System.in);

        // 1. wczytanie pierwszej liczby (musimy o nią zapytać - "wpisz liczbę pierwszą")
        System.out.println("Podaj liczbę pierwszą:");
        int liczba1 = (int) konsola.nextDouble();
        // 2. wczytaj działanie (jako string) - "wpisz działanie [+, -, /, *]" //
        // scanner.next()
        System.out.println("Podaj działanie [+, -, *, /]:");
        String dzialanie = konsola.next();

        // 3. wczytanie drugiej liczby (poprzedzone komunikatem "wpisz drugą liczbę")
        System.out.println("Podaj liczbę drugą:");
        int liczba2 = (int) konsola.nextDouble();
        // // if -

        if (dzialanie.equals("+")) {
            System.out.println("Wynik : " + (liczba1 + liczba2));
            // wypisz wynik dodawania
        } else if (dzialanie.equals("-")) {
            System.out.println("Wynik : " + (liczba1 - liczba2));
            //....
        } else if (dzialanie.equals("*")) {
            System.out.println("Wynik : " + (liczba1 * liczba2));
            //....
        } else if (dzialanie.equals("/") /*&& liczba2 != 0.0*/) {
//            if (liczba2 == 0.0) {
//                System.out.println("Pamiętaj cholero...");
//            } else {
            System.out.println("Wynik : " + (liczba1 / liczba2));
//            }
        } else {
            System.out.println("Niepoprawne działanie kalkulatora");
        }

    }
}
