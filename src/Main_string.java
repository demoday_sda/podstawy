public class Main_string {
    public static void main(String[] args) {
        String pojedynczyCiagZnakow = "Hello world";

        String[] tablicaStringow = new String[]{
                "Element 1",            //0
                "Element 2",            //1
                "Inny ciąg znakow",     //2
                "Ania",                 //3
                "Ela",                  //4
                "Michał",               //5
                "Marek",                //6
                "Zosia"                 //7
        };

        // wypisz wszystkie elementy tablicy pętlą for
        // tablicaStringow.length - rozmiar tablicy . Każda tablica posiada pole
        // length które zawiera rozmiar tablicy
        for (int i = 0; i < tablicaStringow.length; i++) {
            System.out.println(tablicaStringow[i]);
        }

        System.out.println(); // spowoduje dopisanie pustej linii (do oddzielenia pętli)s
        for (int i = 0; i < tablicaStringow.length; i++) {
            // tablicaStringow[i] - podstawowy ciąg znaków
            // tablicaStringow[i].toUpperCase() - tekst zamieniony na wielkie litery
            System.out.println(tablicaStringow[i].toUpperCase());
        }

        // ddzielam
        System.out.println(); //- kolejna petla
        for (int i = 0; i < tablicaStringow.length; i++) {
            System.out.println(tablicaStringow[i].toLowerCase());
        }

        System.out.println(); // kolejna pętla
        for (int i = 0; i < tablicaStringow.length; i++) {
            System.out.println("Tekst: "+tablicaStringow[i]+" - " + tablicaStringow[i].endsWith("a"));
        }

        // 1. wypisz ta samą tablicę i zamień wszystkie znaki na małe litery
        // 2. wypisz tą samą tablicę i sprawdź czy tekst kończy się literką 'a' -
        // do wykonania zadania drugiego posłuż się metodą .endsWith() która
        // w nawiasach przyjmuje tekst którego szukamy

        // wpisz tekst (zad 2) w formacie "Tekst: [TEKST] - true" lub zamiast true - false
    }
}
