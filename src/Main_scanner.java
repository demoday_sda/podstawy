import java.util.Scanner;

public class Main_scanner {
    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);

        System.out.println("Wpisz coś:");
        String tekstOdUzytkownika = skaner.nextLine();

        System.out.println("Wpisałeś/łaś: " + tekstOdUzytkownika);

        // pobierz od użytkownika 2 liczby do zmiennych "zmienna1" , "zmienna2"
        // po pobraniu wypisz na ekran wynik mnożenia obu liczb
        System.out.println("Podaj liczbę 1:");
        int zmienna1 = skaner.nextInt();

        System.out.println("Podaj liczbę 2:");
        int zmienna2 = skaner.nextInt();

        System.out.println("Wynik mnożenia: " + (zmienna1*zmienna2));

    }
}
