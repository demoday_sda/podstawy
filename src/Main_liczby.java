public class Main_liczby {
    public static void main(String[] args) {
        // deklaracja zmiennej o nazwie 'liczba'
        int liczba;

        // inicjalizacja = przypisanie wartości do zmiennej
        liczba = 50;

        // deklaracja oraz inicjalizacja w jednej instrukcji
        int deklaracjaZmiennej = 60;

        System.out.println("Liczba " + liczba);

        // int i = 0; - pierwsza część - inicjalizacja - wykonuje się tylko razs
        // i < 50; - warunek - sprawdzany przed każdym obiegiem pętli
        // i++ -
        for (int i = 0; i < 50; i++) {
            // wykonaj kod wewnątrz klamer
            System.out.println("Wykoanie numer: " + i);
        }
    }
}
